from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^(?P<username>[\w.@+-]+)/ban/$',
        view=views.BanUserView.as_view(),
        name='ban'
    ),
    url(
        regex=r'^(?P<username>[\w.@+-]+)/unban/$',
        view=views.UnbanUserView.as_view(),
        name='unban'
    ),
    url(
        regex=r'^$',
        view=views.UserListView.as_view(),
        name='list'
    ),
    url(
        regex=r'^~redirect/$',
        view=views.UserRedirectView.as_view(),
        name='redirect'
    ),
    url(
        regex=r'^(?P<username>[\w.@+-]+)/$',
        view=views.UserDetailView.as_view(),
        name='detail'
    ),
    url(
        regex=r'^~update/$',
        view=views.UserUpdateView.as_view(),
        name='update'
    ),

]
