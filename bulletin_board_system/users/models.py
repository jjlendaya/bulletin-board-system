from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from model_utils.choices import Choices


@python_2_unicode_compatible
class User(AbstractUser):
    GENDER_CHOICES = Choices(
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other'),
        ('not_specified', 'Not Specified')
    )
    REQUIRED_FIELDS = AbstractUser.REQUIRED_FIELDS + ['about_myself',
                                                      'date_of_birth',
                                                      'hometown',
                                                      'present_location']

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_('Name of User'), blank=True, max_length=255)
    about_myself = models.TextField(max_length=1000)
    date_of_birth = models.DateField()
    hometown = models.CharField(max_length=200)
    present_location = models.CharField(max_length=200)
    skype = models.CharField(max_length=200, null=True, blank=True)
    website = models.URLField(null=True, blank=True)
    gender = models.CharField(choices=GENDER_CHOICES, default=GENDER_CHOICES.not_specified, max_length=20)
    interests = models.TextField(max_length=300, null=True, blank=True)
    banned = models.BooleanField(default=False)

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})
