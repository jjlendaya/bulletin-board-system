from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView, View
from django.http import HttpResponseForbidden
from django.shortcuts import redirect

from django.contrib.auth.mixins import LoginRequiredMixin
from braces import views

from bulletin.permissions import user_is_active_moderator_or_admin

from .models import User


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})


class UserUpdateView(LoginRequiredMixin, UpdateView):

    fields = ['name', ]

    # we already imported User in the view code above, remember?
    model = User

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})

    def get_object(self):
        # Only get the User record for the user making the request
        return User.objects.get(username=self.request.user.username)


class UserListView(LoginRequiredMixin, ListView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'


class BanUserView(View):

    def post(self, request, *args, **kwargs):
        user_to_ban = User.objects.get(username=self.kwargs.get('username'))
        if request.user == user_to_ban:
            return HttpResponseForbidden('You are not allowed to ban yourself.')
        if not (request.user.is_staff or request.user.is_superuser):
            return HttpResponseForbidden('You must be a moderator or administrator to ban others.')
        user_to_ban.banned = True
        user_to_ban.save()
        print("HELLO")
        return redirect('users:detail', username=user_to_ban.username)


class UnbanUserView(View):

    def post(self, request, *args, **kwargs):
        user_to_ban = User.objects.get(username=self.kwargs.get('username'))
        if request.user == user_to_ban:
            return HttpResponseForbidden('You are not allowed to unban yourself.')
        if not (request.user.is_staff or request.user.is_superuser):
            return HttpResponseForbidden('You must be a moderator or administrator to unban others.')
        user_to_ban.banned = False
        user_to_ban.save()
        return redirect('users:detail', username=user_to_ban.username)

