def user_is_active_moderator_or_admin(user):
    return not user.banned and (user.is_staff or user.is_superuser)
