from django.contrib import admin

from .forms import BoardForm
from .models import Board, Thread, Post


class BoardAdmin(admin.ModelAdmin):
    readonly_fields = ['user']
    form = BoardForm

    def save_model(self, request, obj, form, change):
        if not change:
            obj.user = request.user
        return super(BoardAdmin, self).save_model(request, obj, form, change)

    def get_form(self, request, *args, **kwargs):
        form = super(BoardAdmin, self).get_form(request, *args, **kwargs)
        form.current_user = request.user
        return form


# Register your models here.
admin.site.register(Board, BoardAdmin)
admin.site.register(Thread)
admin.site.register(Post)
