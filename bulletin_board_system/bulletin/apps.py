from django.apps import AppConfig


class BulletinConfig(AppConfig):
    name = 'bulletin_board_system.bulletin'
