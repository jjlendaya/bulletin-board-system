from django.db import models
from django.db.models import Count, Sum
from django.contrib.auth import get_user_model
from markdownx.models import MarkdownxField
from django.core.exceptions import ValidationError


# Create your models here.
class Board(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering = ('-created_at', 'name')

    def __str__(self):
        return self.name

    def get_thread_count(self):
        return self.thread_set.count()

    def get_post_count(self):
        threads = self.thread_set.annotate(num_posts=Count('post'))
        if threads:
            return threads.aggregate(Sum('num_posts')).get('num_posts__sum')
        else:
            return 0


class Thread(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    sticky = models.BooleanField(default=False)
    locked = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering = ('-created_at', 'title')

    def __str__(self):
        return self.title

    def get_post_count(self):
        return self.post_set.count()

    def get_last_reply(self):
        posts = self.post_set.all().order_by('-created_at')
        if posts.count() > 0:
            return posts[0]
        else:
            return None


class Post(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    message = MarkdownxField()
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE)

    class Meta:
        ordering = ('created_at', )

    def __str__(self):
        return '{0.thread} - {0.created_at}'.format(self)

    def get_user_post_count(self):
        return self.user.post_set.count()
