from django.conf.urls import url

from . import views

app_name = 'bulletin'
urlpatterns = [
    url(r'^$', views.BoardListView.as_view(), name='board-list'),
    url(r'^board/(?P<pk>\d+)/$', views.ThreadListView.as_view(), name='thread-list'),
    url(r'^thread/(?P<pk>\d+)/$', views.PostListView.as_view(), name='post-list'),
    url(r'^thread/(?P<pk>\d+)/sticky$', views.StickyThreadView.as_view(), name='thread-sticky'),
    url(r'^thread/(?P<pk>\d+)/unsticky$', views.UnstickyThreadView.as_view(), name='thread-unsticky'),
    url(r'^thread/(?P<pk>\d+)/lock', views.LockThreadView.as_view(), name='thread-lock'),
    url(r'^thread/(?P<pk>\d+)/unlock', views.UnlockThreadView.as_view(), name='thread-unlock'),
]
