from django.forms import ModelForm
from markdownx.fields import MarkdownxFormField
from django.core.exceptions import ValidationError

from .models import Thread, Post, Board


class BoardForm(ModelForm):

    def clean(self):
        if self.current_user.banned:
            raise ValidationError('You are banned! You cannot create boards.')
        return self.cleaned_data


class ThreadForm(ModelForm):
    class Meta:
        model = Thread
        fields = ('title', )


class PostForm(ModelForm):
    message = MarkdownxFormField()

    class Meta:
        model = Post
        fields = ('message', )
