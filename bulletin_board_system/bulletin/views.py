from django.views.generic import ListView, View
from django.views.generic.edit import FormMixin
from django.http import HttpResponseForbidden
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.db.models import Max
from braces.views import LoginRequiredMixin, UserPassesTestMixin

from .models import Board, Thread, Post
from .forms import ThreadForm, PostForm
from .permissions import user_is_active_moderator_or_admin


LOGIN_URL = '/accounts/login/'


# Create your views here.
class BoardListView(ListView):
    model = Board


class ThreadListView(LoginRequiredMixin, FormMixin, ListView):
    model = Thread
    form_class = ThreadForm
    paginate_by = 2
    login_url = LOGIN_URL
    redirect_unauthenticated_users = True

    def get_queryset(self):
        """
        Return only the non-sticky threads for pagination. Sticky threads
        must always be on top.
        """
        board = get_object_or_404(Board, pk=self.kwargs.get('pk'))
        threads = board.thread_set.filter(sticky=False)
        threads = threads.annotate(last_post_time=Max('post__created_at'))
        return threads.order_by('-last_post_time')

    def get_context_data(self, **kwargs):
        context = super(ThreadListView, self).get_context_data(**kwargs)
        board = get_object_or_404(Board, pk=self.kwargs.get('pk'))
        context['board'] = board
        context['sticky_threads'] = board.thread_set.filter(sticky=True)
        context['form'] = self.get_form()
        return context

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden('You must be logged in to perform this action.')
        form = self.get_form()
        if request.user.banned:
            return HttpResponseForbidden('You are banned! You cannot create threads or posts.')
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse('bulletin:thread-list', kwargs={'pk': self.kwargs.get('pk')})

    def form_valid(self, form):
        new_thread = form.save(commit=False)
        new_thread.user = self.request.user
        board = get_object_or_404(Board, pk=self.kwargs.get('pk'))
        new_thread.board = board
        new_thread.save()
        return super(ThreadListView, self).form_valid(form)


class PostListView(LoginRequiredMixin, FormMixin, ListView):
    model = Post
    form_class = PostForm
    paginate_by = 2
    login_url = LOGIN_URL
    redirect_unauthenticated_users = True

    def get_queryset(self):
        """
        Filter by the thread
        """
        thread = get_object_or_404(Thread, pk=self.kwargs.get('pk'))
        return thread.post_set.all()

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        thread = get_object_or_404(Thread, pk=self.kwargs.get('pk'))
        context['thread'] = thread
        context['form'] = self.get_form()
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if request.user.banned:
            return HttpResponseForbidden('You are banned! You cannot create threads or posts.')
        thread_pk = self.kwargs.get('pk')
        thread = get_object_or_404(Thread, pk=thread_pk)
        if not thread.locked:
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)
        else:
            return HttpResponseForbidden('You cannot create posts on a locked thread.')

    def get_success_url(self):
        return reverse('bulletin:post-list', kwargs={'pk': self.kwargs.get('pk')})

    def form_valid(self, form):
        new_post = form.save(commit=False)
        new_post.user = self.request.user
        thread = get_object_or_404(Thread, pk=self.kwargs.get('pk'))
        new_post.thread = thread
        new_post.save()
        return super(PostListView, self).form_valid(form)


class LockThreadView(LoginRequiredMixin,
                     UserPassesTestMixin,
                     View):
    login_url = LOGIN_URL
    redirect_unauthenticated_users = True

    def test_func(self, user):
        return user_is_active_moderator_or_admin(user)

    def post(self, request, *args, **kwargs):
        thread_pk = self.kwargs.get('pk')
        thread = get_object_or_404(Thread, pk=thread_pk)
        thread.locked = True
        thread.save()
        return redirect('bulletin:post-list', pk=thread_pk)


class UnlockThreadView(LoginRequiredMixin,
                       UserPassesTestMixin,
                       View):
    login_url = LOGIN_URL
    redirect_unauthenticated_users = True

    def test_func(self, user):
        return user_is_active_moderator_or_admin(user)

    def post(self, request, *args, **kwargs):
        thread_pk = self.kwargs.get('pk')
        thread = get_object_or_404(Thread, pk=thread_pk)
        thread.locked = False
        thread.save()
        return redirect('bulletin:post-list', pk=thread_pk)


class StickyThreadView(LoginRequiredMixin,
                       UserPassesTestMixin,
                       View):
    login_url = LOGIN_URL
    redirect_unauthenticated_users = True

    def test_func(self, user):
        return user_is_active_moderator_or_admin(user)

    def post(self, request, *args, **kwargs):
        thread_pk = self.kwargs.get('pk')
        thread = get_object_or_404(Thread, pk=thread_pk)
        thread.sticky = True
        thread.save()
        return redirect('bulletin:post-list', pk=thread_pk)


class UnstickyThreadView(LoginRequiredMixin,
                         UserPassesTestMixin,
                         View):
    login_url = LOGIN_URL
    redirect_unauthenticated_users = True

    def test_func(self, user):
        return user_is_active_moderator_or_admin(user)

    def post(self, request, *args, **kwargs):
        thread_pk = self.kwargs.get('pk')
        thread = get_object_or_404(Thread, pk=thread_pk)
        thread.sticky = False
        thread.save()
        return redirect('bulletin:post-list', pk=thread_pk)
